#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests
import time


OH_URL = 'http://api.open-notify.org/iss-pass.json'

def main():
    """your code goes below here"""
    
    # stuck? you can always write comments
    # Try describe the steps you would take manually
    
    print('Enter Lat')
    lat_input = input()
    print('Enter Lon')
    lon_input = input()

    resp = requests.get(f'{OH_URL}?lat={lat_input}&lon={lon_input}')
    if resp.status_code == 200:
        d_resp = resp.json()
        for t in d_resp['response']:
            print(f"Time: {time.ctime(t['risetime'])}")
    else:
        print(f'Status Code: {resp.status_code}')


if __name__ == "__main__":
    main()

