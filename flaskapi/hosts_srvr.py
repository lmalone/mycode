#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flask import render_template

app = Flask(__name__)

app.secret_key= "random random RANDOM!"

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]

@app.route("/hosts", methods = ["POST", "GET"])
def hosts():
    if request.method == "GET":
        return render_template("hosts.j2", hosts = groups)
    elif request.method == "POST":
        groups.append({'hostname': request.form.get("hostname", None),
            'ip': request.form.get('ip', None), 'fqdn': request.form.get('fqdn', None)})
        return render_template("hosts.j2", hosts = groups)


if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224)
