#!/usr/bin/python3

import requests

# define the URL we want to use
TIMEURL = "http://time.jsontest.com/"
IPURL = "http://ip.jsontest.com"
VALIDATEURL = "http://validate.jsontest.com"

def main():

    mytime = ''
    myip = ''

    # Send GET request for time
    time_resp = requests.get(TIMEURL)
    if time_resp.status_code == 200:
        d_time = time_resp.json()
        mytime = d_time['time']

    # Send GET request for IP
    ip_resp = requests.get(IPURL)
    if ip_resp.status_code == 200:
        d_ip = ip_resp.json()
        myip = d_ip['ip']

    # Read servers from local file
    with open("myservers.txt") as f:
        servers = f.readlines()

    # Build dictionary representing JSON to validate (using appropriate quotes)
    this_json = {}
    this_json["time"] = mytime
    this_json["ip"] = myip
    this_json["servers"] = servers

    # Build dictionary representing JSON form data to send as param. Add JSON string to be validated
    this_data = {}
    this_data["json"] = str(this_json)

    # Send POST to validate JSON
    validate_resp = requests.post(VALIDATEURL, data=this_data)
    
    # Convert JSON to Python dictionary and print
    if validate_resp.status_code == 200:
        resp_json = validate_resp.json()
        print(resp_json)

    print(f"ANSWER: {resp_json['validate']}")

    print(f"Is your JSON valid? {resp_json['validate']}")

if __name__ == "__main__":
    main()

